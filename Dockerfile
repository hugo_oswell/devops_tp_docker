FROM node:13.13.0-stretch
WORKDIR /app
COPY ./ ./
RUN yarn install
ENTRYPOINT [ "yarn", "start" ]
