import React from "react";

const NavHorizontalSeparator = () => {
  return <span className="h-05 w-48 mt-6 light-grey-bg rounded-sm"></span>;
};

export default NavHorizontalSeparator;
