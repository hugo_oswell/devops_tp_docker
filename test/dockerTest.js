const assert = require("chai").assert;
const app = require("../dummyMethod");

describe("App", () => {
  it("app should return hello", () => {
    assert.equal(app(), "Hello");
  });
});
